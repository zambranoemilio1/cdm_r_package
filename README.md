# cdm_r_package

R package used to perform data mining tasks on univariate time series based on the CDM technique (https://www.cs.ucr.edu/~eamonn/SIGKDD_2004_long.pdf)