This document explains each folder and document in the repository in order to develop the package in an orderly manner.

* analisys and design
	- analisys and design.html
		It contains the functional and non-functional requirements.

	- requirements verifications.html
		It contains a table to verify the status of the requirements.
	
	- SIGKDD_2004_long.pdf
	 	Original scientific publication describing the CDM technique.


* cdm
	R package that contains the implementation of CDM and its use in Time Series Data Mining.


* data
	- links_to_data.txt
		It contains all the links to the data used to work in development and testing of the package. The data is not put into the repository to avoid using too much storage space. This data is generally used by the scientific community to test Data Mining techniques.
		

* guides
	- comments_to_repo.pdf
		Comments to upload code to the repository will be handled according to the recommendations made in the article by chris beams (https://chris.beams.io/posts/git-commit/)

	- r_style_guide_tidyverse.pdf
		The tidyverse style guide (https://style.tidyverse.org/) is used.
		The "styler" package is used to ensure compliance with the tidyverse style guide. To install styler: 
			install.packages("styler")

	- semantic_versioning.pdf
		Indicates the versioning used in the creation of the package (MAJOR.MINOR.PATCH).


* spikes
	- Store code used to test concepts that are important but not part of the package.
	- There are also suggestion templates for unit tests, comments to repo, functions, and more.


