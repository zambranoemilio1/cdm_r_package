#include <Rcpp.h>
using namespace Rcpp;

// This is only a dummy function so that the package recognizes the structure that 
// there will be code made in C ++
// [[Rcpp::export]]
NumericVector timesTwo(NumericVector x) {
  return x * 2;
}


